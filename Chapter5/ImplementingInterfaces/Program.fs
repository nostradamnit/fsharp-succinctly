﻿open System.Web.Security
// give shorte name to password hashing method
let hash = FormsAuthentication.HashPasswordForStoringInConfigFile

// an interface "IUser" 
type IUser =
    // hashs the users password and checks it against
    // the known hash
    abstract Authenticate: evidence: string -> bool
    // gets the users logon message
    abstract LogonMessage: unit -> string

// a class that represents a user
// it's constructor takes two parameters, the user's 
// name and a hash of their password
type User(name, passwordHash) =
    interface IUser with
        // Authenticate implementation
        member x.Authenticate(password) =
            let hashResult = hash (password, "sha1")
            passwordHash = hashResult

        // LogonMessage implementation
        member x.LogonMessage() =
            Printf.sprintf "Hello, %s" name

// create a new instance of the user
let user = User("Robert", "AF73C586F66FDC99ABF1EADB2B71C5E46C80C24A")
// cast to the IUser interface
let iuser = user :> IUser
// get the logon message
let logonMessage = iuser.LogonMessage()

let logon (iuser: IUser) =
    // authenticate user and print appropriate message
    if iuser.Authenticate("badpass") then
        printfn "%s" logonMessage
    else
        printfn "Logon failed"    

do logon user