﻿open System
open System.Windows
open System.Windows.Controls

// adds a child to a panel control
let addChild child (control: Panel) =
    control.Children.Add child |> ignore

    // function to create the form interface
let createForm() =
    // horizontal stack panel to hold label and text box
    let spHorozontal = 
        new StackPanel(Orientation = Orientation.Horizontal)

    // add the label to the stack panel
    spHorozontal |> addChild (new Label(Content = "A field",
                                        Width = 100.))

    // add a text box to the stack panel
    let text = new TextBox(Text = "<enter something>")
    spHorozontal |> addChild text

    // create a second stack panel to hold our label
    // and a text box with a button below it
    let spVert = new StackPanel()
    spVert |> addChild spHorozontal

    // create the button and make it show the contents
    // of the text box when pressed
    let button = new Button(Content= "Press me!")
    button.Click.Add(fun _ -> MessageBox.Show text.Text |> ignore)
    spVert |> addChild button

    // return the outer most stack panel
    spVert

    // create the window that will hold the controls
let win = new Window(Content = createForm(), 
                     Title = "A simple form",
                     Width = 300., Height = 150.)

// create the application object and show the window
let app = new Application()
[<STAThread>]
do app.Run win |> ignore
