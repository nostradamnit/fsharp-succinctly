﻿open System
open System.Windows
open System.Windows.Controls
open System.Windows.Media
open System.Windows.Threading

/// Represents a ball by its position and its velocity
type Ball = 
    { X: float
      Y: float
      XVel: float
      YVel: float }

      /// convient function to create a new instance of a ball
        static member New(x: float, y: float, xVel: float, yVel: float) = 
            { X = x
              Y = y
              XVel = xVel
              YVel = yVel }

        /// Create's a new ball that has moved one step by its velocity within 
        /// the given bounds, if the bounds are crossed the ball velocity is 
        /// inversed
        member ball.Move(xMin: float, yMin: float, xMax: float, yMax: float) =
            // local helper to implement the logic of the movement
            let updatePositionAndVelocity pos vel min max =
                let pos' = pos + vel
                if min < pos' && pos' < max then
                    pos', vel
                else
                    pos - vel, -vel
            // get the new position and velocity
            let newX, newXVel = 
                updatePositionAndVelocity ball.X ball.XVel xMin xMax
            let newY, newYVel = 
                updatePositionAndVelocity ball.Y ball.YVel yMin yMax

            // create the next ball
            Ball.New(newX, newY,newXVel, newYVel)


///WPF BouncingBall Implementation
type BallRender() as br =
    inherit FrameworkElement()

    // ball size and limits
    let size = 5.0
    let xMax = 100.0
    let yMax = 100.0
    
    // offset to give a nice board
    let offset = 10.0
    
    // pen and brush objects
    let brush = new SolidColorBrush(Colors.Black)
    let pen = new Pen(brush,1.0)

      // a reference cell that holds the current ball instance
    let ball = ref (Ball.New(50., 80., 0.75, 1.25))

    // timer for updating the balls position
    let timer = new DispatcherTimer(Interval = new TimeSpan(0,0,0,0,50))

    // helper function to do some initialization
    let init() =
        // set the controls width and height
        br.Width <- (xMax * size) + (offset * 2.0)
        br.Height <- (yMax * size) + (offset * 2.0)

        // set up the timer
        timer.Tick.Add(fun _ ->
            // move the current ball to its next position 
            ball := ball.Value.Move(0.,0., xMax,yMax)
            // invalidate the control to force a redraw
            br.InvalidateVisual())
        timer.Start()
    
    do init()


     /// function that takes care of actually drawing the ball
    override x.OnRender(dc: DrawingContext) =
        // calculate the balls position on the canvas
        let x = (ball.Value.X * size) + offset
        let y = (ball.Value.Y * size) + offset

        // draw the ball and an outline rectangle
        dc.DrawEllipse(brush, pen, new Point(x, y), size, size)
        dc.DrawRectangle(null, pen, new Rect(offset, offset, size * xMax, size * xMax))

  module Main =    
    // create an instance of the new control
    let br = new BallRender()
    // create a window to hold the control
    let win = new Window(Title = "Bouncing Ball", 
                         Content = br, 
                         Width = br.Width + 20., 
                         Height = br.Height + 40.)

    // start the event loop and show control
    let app = new Application()
    [<STAThread>]
    do app.Run win |> ignore